package com.example.employees.dao.mapper;

import com.example.employees.domain.Company;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CompanyMapper implements RowMapper<Company> {
    @Override
    public Company mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Company(resultSet.getLong("id"),
                resultSet.getString("name"),
                resultSet.getString("inn"),
                resultSet.getString("address"),
                resultSet.getString("phone")
        );
    }

    public SqlParameterSource getParameterMap(Company company) {
        return new MapSqlParameterSource()
                .addValue("id", company.getId())
                .addValue("name", company.getName())
                .addValue("inn", company.getInn())
                .addValue("address", company.getAddress())
                .addValue("phone", company.getPhone());
    }
}
