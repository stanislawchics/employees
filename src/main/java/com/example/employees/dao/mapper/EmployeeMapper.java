package com.example.employees.dao.mapper;

import com.example.employees.domain.Employee;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Component
public class EmployeeMapper implements RowMapper<Employee> {
    @Override
    public Employee mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Employee(resultSet.getLong("id"),
                resultSet.getString("fullname"),
                resultSet.getDate("birthday") == null ?
                        null : resultSet.getDate("birthday").toLocalDate(),
                resultSet.getString("email"),
                resultSet.getLong("company_id")
        );
    }

    public SqlParameterSource getParameterMap(Employee employee) {
        return new MapSqlParameterSource()
                .addValue("id", employee.getId())
                .addValue("fullname", employee.getFullName())
                .addValue("birthday", employee.getBirthday())
                .addValue("email", employee.getEmail())
                .addValue("company_id", employee.getCompanyId());
    }
}
