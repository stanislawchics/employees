package com.example.employees.dao;

import com.example.employees.domain.Company;

import java.util.Collection;
import java.util.List;

public interface CompanyDao {
    List<Company> getCompanies();
    List<Company> getCompaniesSortedByName();
    List<Company> getCompaniesById(List<Long> ids);
    int updateOrInsertCompany(Company company);
    int removeCompanies(List<Long> ids);
    List<Company> findCompanies(String searchStr);
}