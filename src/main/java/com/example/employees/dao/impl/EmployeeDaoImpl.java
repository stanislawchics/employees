package com.example.employees.dao.impl;

import com.example.employees.dao.EmployeeDao;
import com.example.employees.dao.mapper.EmployeeMapper;
import com.example.employees.domain.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {
    private static final String SELECT_ALL_QUERY = "SELECT id, fullname, birthday, email, company_id FROM employees";
    private static final String SELECT_BY_IDS_QUERY = "SELECT id, fullname, birthday, email, company_id FROM employees WHERE id IN (:ids)";
    private static final String SELECT_BY_COMPANIES_QUERY = "SELECT id, fullname, birthday, email, company_id FROM employees WHERE company_id IN (:companiesIds)";
    private static final String FIND_QUERY = "SELECT id, fullname, birthday, email, company_id FROM employees WHERE " +
            "lower(fullname) LIKE :searchStr OR " +
            "lower(email) like :searchStr";
    private static final String FIND_IN_COMPANIES_QUERY = "SELECT * FROM employees WHERE " +
            "company_id in(:companiesIds) AND " +
            "(lower(fullname) LIKE :searchStr OR " +
            "lower(email) like :searchStr)";
    private static final String UPDATE_QUERY = "UPDATE employees " +
            "SET fullname=:fullname, birthday=:birthday, email=:email, company_id=:company_id WHERE id=:id";
    private static final String INSERT_QUERY = "INSERT INTO employees (fullname, birthday, email, company_id) " +
            "VALUES (:fullname, :birthday, :email, :company_id)";
    private static final String DELETE_QUERY = "DELETE FROM employees WHERE id IN(:ids)";

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final EmployeeMapper mapper;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public EmployeeDaoImpl(JdbcTemplate jdbcTemplate, EmployeeMapper mapper) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        this.mapper = mapper;
    }
    @Override
    public List<Employee> getEmployees() {
        try{
            return namedParameterJdbcTemplate.query(SELECT_ALL_QUERY, mapper);
        } catch (Exception e) {
            logger.error("Ошибка при выполнении SELECT_ALL_QUERY", e);
            return null;
        }
    }

    @Override
    public List<Employee> getEmployeesById(List<Long> ids) {
        try {
            return namedParameterJdbcTemplate.query(SELECT_BY_IDS_QUERY, new MapSqlParameterSource("ids", ids), mapper);
        } catch (Exception e) {
            logger.error("Ошибка при выполнении SELECT_BY_IDS_QUERY", e);
            return null;
        }
    }

    @Override
    @Transactional
    public int updateOrInsertEmployee(Employee employee) {
        try {
            if (employee.getId() != null) {
                return namedParameterJdbcTemplate.update(UPDATE_QUERY, mapper.getParameterMap(employee));
            } else {
                return namedParameterJdbcTemplate.update(INSERT_QUERY, mapper.getParameterMap(employee));
            }
        } catch (Exception e) {
            logger.error("Ошибка при выполнении UPDATE_QUERY или INSERT_QUERY", e);
            return -1;
        }
    }

    @Override
    public int removeEmployees(List<Long> ids) {
        try {
            return namedParameterJdbcTemplate.update(DELETE_QUERY,  new MapSqlParameterSource("ids", ids));
        } catch (Exception e) {
            logger.error("Ошибка при выполнении DELETE_QUERY", e);
            return -1;
        }
    }

    @Override
    public List<Employee> findEmployee(String searchStr) {
        try {
            searchStr = searchStr.toLowerCase();
            return namedParameterJdbcTemplate.query(FIND_QUERY,
                    new MapSqlParameterSource("searchStr", "%" + searchStr + "%"),
                    mapper);
        } catch (Exception e) {
            logger.error("Ошибка при выполнении FIND_QUERY", e);
            return null;
        }
    }

    @Override
    public List<Employee> getEmployeesByCompanies(List<Long> companiesIds) {
        try {
            return namedParameterJdbcTemplate.query(SELECT_BY_COMPANIES_QUERY, new MapSqlParameterSource("companiesIds", companiesIds), mapper);
        } catch (Exception e) {
            logger.error("Ошибка при выполнении SELECT_BY_COMPANIES_QUERY", e);
            return null;
        }
    }

    @Override
    public List<Employee> findEmployeeInCompanies(List<Long> companiesIds, String searchStr) {
        try {
            searchStr = searchStr.toLowerCase();
            MapSqlParameterSource paramMap = new MapSqlParameterSource();
            paramMap.addValue("companiesIds", companiesIds);
            paramMap.addValue("searchStr", "%" + searchStr + "%");
            return namedParameterJdbcTemplate.query(FIND_IN_COMPANIES_QUERY, paramMap, mapper);
        } catch (Exception e) {
            logger.error("Ошибка при выполнении FIND_IN_COMPANIES_QUERY", e);
            return null;
        }
    }
}
