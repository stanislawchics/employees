package com.example.employees.dao.impl;

import com.example.employees.dao.CompanyDao;
import com.example.employees.dao.mapper.CompanyMapper;
import com.example.employees.domain.Company;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class CompanyDaoImpl implements CompanyDao {
    private static final String SELECT_ALL_QUERY = "SELECT id, name, inn, address, phone FROM companies";
    private static final String SELECT_ALL_ORDER_BY_NAME_QUERY = "SELECT id, name, inn, address, phone FROM companies ORDER BY name";
    private static final String SELECT_BY_IDS_QUERY = "SELECT id, name, inn, address, phone FROM companies WHERE id IN (:ids)";
    private static final String FIND_QUERY = "SELECT * FROM companies WHERE " +
            "lower(name) LIKE :searchStr OR " +
            "lower(inn) like :searchStr OR " +
            "lower(address) like :searchStr OR " +
            "lower(phone) like :searchStr";
    private static final String UPDATE_QUERY = "UPDATE companies " +
            "SET name=:name, inn=:inn, address=:address, phone=:phone WHERE id=:id";
    private static final String INSERT_QUERY = "INSERT INTO companies (name, inn, address, phone) " +
            "VALUES (:name, :inn, :address, :phone)";
    private static final String DELETE_QUERY = "DELETE FROM companies WHERE id IN(:ids)";

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final CompanyMapper mapper;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public CompanyDaoImpl(JdbcTemplate jdbcTemplate, CompanyMapper mapper) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        this.mapper = mapper;
    }

    @Override
    public List<Company> getCompanies() {
        try {
            return namedParameterJdbcTemplate.query(SELECT_ALL_QUERY, mapper);
        } catch (Exception e) {
            logger.error("Ошибка при выполнении SELECT_ALL_QUERY", e);
            return null;
        }
    }

    @Override
    public List<Company> getCompaniesSortedByName() {
        try {
            return namedParameterJdbcTemplate.query(SELECT_ALL_ORDER_BY_NAME_QUERY, mapper);
        } catch (Exception e) {
            logger.error("Ошибка при выполнении SELECT_ALL_ORDER_BY_NAME_QUERY", e);
            return null;
        }
    }

    @Override
    public List<Company> getCompaniesById(List<Long> ids) {
        try {
            return namedParameterJdbcTemplate.query(SELECT_BY_IDS_QUERY, new MapSqlParameterSource("ids", ids), mapper);
        } catch (Exception e) {
            logger.error("Ошибка при выполнении SELECT_BY_IDS_QUERY", e);
            return null;
        }
    }

    @Override
    @Transactional
    public int updateOrInsertCompany(Company company) {
        try {
            if(company.getId() != null) {
                return namedParameterJdbcTemplate.update(UPDATE_QUERY, mapper.getParameterMap(company));
            } else {
                return namedParameterJdbcTemplate.update(INSERT_QUERY, mapper.getParameterMap(company));
            }
        } catch (Exception e) {
            logger.error("Ошибка при выполнении UPDATE_QUERY или INSERT_QUERY", e);
            return -1;
        }
    }

    @Override
    public int removeCompanies(List<Long> ids) {
        try {
            return namedParameterJdbcTemplate.update(DELETE_QUERY, new MapSqlParameterSource("ids", ids));
        } catch (Exception e) {
            logger.error("Ошибка при выполнении DELETE_QUERY", e);
            return -1;
        }
    }

    @Override
    public List<Company> findCompanies(String searchStr){
        try {
            searchStr = searchStr.toLowerCase();
            return namedParameterJdbcTemplate.query(FIND_QUERY,
                    new MapSqlParameterSource("searchStr", "%" + searchStr + "%"),
                    mapper);
        } catch (Exception e) {
            logger.error("Ошибка при выполнении FIND_QUERY", e);
            return null;
        }
    }
}
