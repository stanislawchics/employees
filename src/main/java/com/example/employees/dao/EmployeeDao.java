package com.example.employees.dao;

import com.example.employees.domain.Employee;

import java.util.Collection;
import java.util.List;

public interface EmployeeDao {
    List<Employee> getEmployees();
    List<Employee> getEmployeesById(List<Long> ids);
    int updateOrInsertEmployee(Employee employee);
    int removeEmployees(List<Long> ids);
    List<Employee> findEmployee(String searchStr);
    List<Employee> getEmployeesByCompanies(List<Long> companiesIds);
    List<Employee> findEmployeeInCompanies(List<Long> companiesIds, String searchStr);
}
