package com.example.employees.vaadin.component.grid;

import com.example.employees.dao.CompanyDao;
import com.example.employees.domain.Company;
import com.example.employees.vaadin.component.editor.CompanyModalEditor;
import com.example.employees.vaadin.component.validation.FieldFormatters;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.selection.SelectionListener;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Класс отображающий список компаний и реализующий все методы {@link EntitiesTab}. Использует {@link CompanyDao} для доступа к данным
 */
@SpringComponent
@UIScope
public class CompanyEntitiesGrid extends Grid<Company> implements EntitiesTab<Company> {
    /** Заголовок для таба {@link EntitiesTab#getTitle()} */
    @Getter
    private final String title = "Компании";
    /** Источник данных о компаниях */
    private CompanyDao companyDao;
    /** Компонент для редактирования компании */
    private CompanyModalEditor editor;
    /** Фильтр по компаниями {@link EntitiesTab#setFilter(String)} */
    private String filter = "";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void setFilter(String filter) {
        if (!isVisible())
            return;
        if (filter != null && !filter.equals(this.filter)) {
            this.filter = filter;
            refillData();
        }
    }

    @Autowired
    public CompanyEntitiesGrid(CompanyDao companyDao, CompanyModalEditor editor) {
        super(Company.class);
        this.companyDao = companyDao;
        this.editor = editor;
        setColumns();
        setSelectionMode(SelectionMode.MULTI);
    }

    private void setColumns() {
        removeAllColumns();
        addColumn(Company::getName).setHeader("Название");
        addColumn(Company::getInn).setHeader("ИНН");
        addColumn(Company::getAddress).setHeader("Адрес");
        addColumn(c -> c.getPhone().isEmpty() ? "" : FieldFormatters.formatPhoneString(c.getPhone())).setHeader("Телефон");
    }

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible)
            refillData();
        super.setVisible(visible);
    }

    @Override
    public void refillData() {
        if (filter == null || filter.isEmpty()) {
            List<Company> companies = companyDao.getCompanies();
            if (companies == null) {
                Notification.show("При получении списка компаний произошла ошибка", 2000, Notification.Position.MIDDLE);
                logger.error("Ошибка при получении списка компаний без фильтра");
            } else {
                setItems(companies);
            }
        } else {
            List<Company> companies = companyDao.findCompanies(filter);
            if (companies == null) {
                Notification.show("При получении списка компаний произошла ошибка", 2000, Notification.Position.MIDDLE);
                logger.error("Ошибка при получении списка компаний соответствующих фильтру: " + filter);
            } else {
                setItems(companies);
            }
        }
    }

    @Override
    public void onCreateEntity() {
        if (!isVisible())
            return;
        editor.editCompany(new Company(), this);
        editor.open();
    }

    @Override
    public void onEditEntity() {
        if (!isVisible())
            return;
        Set<Company> selectedCompanies = getSelectedItems();
        Company selectedCompany = selectedCompanies.stream().findFirst().get();
        editor.editCompany(selectedCompany, this);
        editor.open();
    }

    @Override
    public void onRemoveEntity() {
        if (!isVisible())
            return;
        confirmToRemoveCompanies(getSelectedItems());
    }


    private void confirmToRemoveCompanies(Set<Company> companiesToRemove) {
        Button yes = new Button("Да");
        Button no = new Button("Отмена");
        Span content;
        if(companiesToRemove.size() == 1) {
            content = new Span("Вы уверены, что хотите удалить выбранную компанию: " +
                    companiesToRemove.stream().findFirst().get().getName() +
                    "?");
        } else {
            content = new Span("Вы уверены, что хотите удалить " +
                    companiesToRemove.size() +
                    " компаний?");
        }
        HorizontalLayout hl = new HorizontalLayout(yes, no);
        VerticalLayout vl = new VerticalLayout(content, hl);
        Notification notification = new Notification(vl);
        yes.addClickListener(event -> {
            int removedCnt = companyDao.removeCompanies(companiesToRemove.stream()
                    .map(Company::getId)
                    .collect(Collectors.toList())
            );
            notification.close();
            refillData();
            if(removedCnt < 0) {
                Notification.show("При удалении компаний произошла ошибка", 2000, Notification.Position.MIDDLE);
                logger.error("Ошибка при удалении компаний: " + companiesToRemove);
            } else {
                Notification.show("Успешно удалено " + removedCnt + " компаний", 2000, Notification.Position.MIDDLE);
                logger.debug("Удалены компании: " + companiesToRemove);
            }
        });
        no.addClickListener(event -> notification.close());
        notification.setPosition(Notification.Position.MIDDLE);
        notification.open();
    }

    @Override
    public Registration addEntitySelectionListener(SelectionListener<Grid<Company>, Company> listener) {
        return super.addSelectionListener(listener);
    }
}
