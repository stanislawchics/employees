package com.example.employees.vaadin.component.i18n;

import com.vaadin.flow.component.datepicker.DatePicker;

import java.util.List;
import java.util.Locale;

public class LocalizedDatePicker extends DatePicker {

    public LocalizedDatePicker(String label) {
        super(label);
        setLocale(new Locale("ru"));
        setI18n(new DatePicker.DatePickerI18n()
                .setWeek("Неделя")
                .setCalendar("Календарь")
                .setClear("Очистить")
                .setToday("Сегодня")
                .setCancel("Отменять")
                .setFirstDayOfWeek(1)
                .setMonthNames(List.of("Январь", "Февраль", "Март",
                        "Апрель", "Май", "Июнь", "Июль", "Август",
                        "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"))
                .setWeekdays(List.of("понедельник", "вторник", "среда",
                        "четверг", "пятница", "суббота", "воскресенье"))
                .setWeekdaysShort(List.of("пн", "вт", "ср", "чт", "пт",
                        "сб", "вс"))
        );
    }
}

