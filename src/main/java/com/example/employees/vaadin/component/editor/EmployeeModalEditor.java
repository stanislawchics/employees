package com.example.employees.vaadin.component.editor;

import com.example.employees.dao.EmployeeDao;
import com.example.employees.domain.Company;
import com.example.employees.domain.Employee;
import com.example.employees.vaadin.component.grid.EntitiesTab;
import com.example.employees.vaadin.component.validation.EmailValidator;
import com.example.employees.vaadin.component.i18n.LocalizedDatePicker;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.List;

/**
 * Класс компонент, отображаемый в модальном окне, и используемый для сохранения нового/редактирования существующего сотрудника
 */
@SpringComponent
@UIScope
public class EmployeeModalEditor extends Dialog {
    /** Binder, связывающий поля модального окна с полями сущности {@link Employee} */
    private Binder<Employee> binder = new Binder<>(Employee.class);
    /** Источник данных о сотрудниках */
    private EmployeeDao employeeDao;
    /** Объект сотрудника используемый {@link EmployeeModalEditor#binder} */
    private Employee employee;
    /** Поле с ФИО нового/редактируемого сотрудника */
    private TextField fullName = new TextField("ФИО");
    /** Выпадающий список с компанией нового/редактируемого сотрудника */
    private ComboBox<Company> company = new ComboBox<>("Компания");
    {
        company.setItemLabelGenerator(Company::getName);
    }
    /**
     * Мапа, используемая для выбора объекта {@link Company} в {@link EmployeeModalEditor#company} по id компании из
     * объекта редактируемого сотрудника. Так же значения мапы используются для отображения в {@link EmployeeModalEditor#company}
     */
    private Map<Long, Company> companies = new HashMap<>();
    /** DatePicker с днем рождения нового/редактируемого сотрудника */
    private DatePicker birthday = new LocalizedDatePicker("День рождения");
    /** Поле с e-mail нового/редактируемого сотрудника */
    private TextField email = new TextField("E-mail");
    /** Кнопка сохранить, вызывает {@link EmployeeModalEditor#saveEmployee()} */
    private Button btnSave = new Button("Сохранить");
    /** Кнопка отменить, закрывающая модальное окно */
    private Button btnCancel = new Button("Отменить");
    private HorizontalLayout btns = new HorizontalLayout(btnSave, btnCancel);
    /** Ссылка на компонент - список сотрудников, используемая для обновления списка после сохранения сотрудника */
    private EntitiesTab employeesGrid;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Конструктор. Содержит вызов метода для разхмещения полей и кнопок,
     * а так же вызов метода для связывания и валидации полей с {@link EmployeeModalEditor#binder}
     * @param employeeDao - источник данных о сотрудниках
     */
    @Autowired
    public EmployeeModalEditor(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
        addButtonsAndTextFields();
        bindFields();
    }

    private void addButtonsAndTextFields() {
        HorizontalLayout employeeFieldsLayout = new HorizontalLayout(fullName, company, email, birthday);
        VerticalLayout layout = new VerticalLayout(employeeFieldsLayout, btns);
        add(layout);
        btnSave.addClickListener(e -> saveEmployee());
        btnCancel.addClickListener(e -> close());
    }

    private void bindFields(){
        binder.forField(fullName)
                .asRequired("ФИО не должно быть пустым")
                .bind(Employee::getFullName, Employee::setFullName);
        binder.forField(company)
                .asRequired("У сотрудника должна быть выбрана компания")
                .bind(e -> companies.get(e.getCompanyId()), (e, company) -> e.setCompanyId(company.getId()));
        binder.forField(birthday)
                .bind(Employee::getBirthday, Employee::setBirthday);
        binder.forField(email)
                .withValidator(new EmailValidator("E-mail не валиден"))
                .bind(Employee::getEmail, Employee::setEmail);
        binder.bindInstanceFields(this);
    }

    private void saveEmployee() {
        if (binder.validate().hasErrors())
            return;
        int updatedCnt = employeeDao.updateOrInsertEmployee(employee);
        if (updatedCnt < 0) {
            String action = employee.getId() == null ? "добавлении" : "изменении";
            Notification.show("При " + action + " сотрудника " + employee.getFullName() + " произошла ошибка",
                    2000,
                    Notification.Position.MIDDLE);
            logger.error("Ошибка при " + action + " сотрудника - " + employee);
        } else {
            String action = employee.getId() == null ? "добавлен" : "изменен";
            Notification.show("Сотрудник " + employee.getFullName() + " успешно " + action,
                    2000,
                    Notification.Position.MIDDLE);
            logger.debug("Сотрудник " + action + ": "+ employee);
        }
        employeesGrid.refillData();
        close();
    }

    /**
     * Метод инициализирует связанный с биндером объект сотрудника
     * @param employee - объект сотрудника. Пустой в случае добавления сотрудника, и, содержащий как минимум id, в случае с редактированием имеющегося
     * @param employeesGrid - {@link EmployeeModalEditor#employeesGrid}
     * @param companies - список возможных компаний
     */
    public void editEmployee(Employee employee, EntitiesTab employeesGrid, Collection<Company> companies) {
        this.employeesGrid = employeesGrid;
        this.companies.clear();
        companies.forEach(company -> this.companies.put(company.getId(), company));
        this.company.setItems(companies);
        if (employee == null) {
            close();
            return;
        }

        if (employee.getId() != null) {
            List<Employee> employees = employeeDao.getEmployeesById(List.of(employee.getId()));
            if (employees == null || employees.size() == 0) {
                Notification.show("Произошла ошибка - редактируемый сотрудник не найден. Пожалуйста, обновите страницу",
                        2000,
                        Notification.Position.MIDDLE);
                logger.error("Ошибка при получении сотрудника по id " + employee.getId());
            } else {
                this.employee = employees.get(0);
            }
        } else {
            this.employee = employee;
        }

        binder.setBean(this.employee);
    }
}
