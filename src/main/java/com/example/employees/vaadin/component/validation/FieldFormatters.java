package com.example.employees.vaadin.component.validation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.vaadin.textfieldformatter.CustomStringBlockFormatter;

@Configuration
public class FieldFormatters {
    public static String formatPhoneString(String phone) {
        phone = phone.replaceAll("[^0-9]", "");
        if(phone.length() != 11)
            throw new IllegalArgumentException("Строка с телефонам должна содержать 11 цифр");
        return phone.substring(0,1) + " (" + phone.substring(1,4) + ") " + phone.substring(4,7) + " " + phone.substring(7,9) + "-" + phone.substring(9);
    }

    @Bean(name = "phoneFieldFormatter")
    public static CustomStringBlockFormatter getPhoneFieldFormatter() {
        CustomStringBlockFormatter.Options options = new CustomStringBlockFormatter.Options();
        options.setBlocks(1,3,3,2,2);
        options.setDelimiters(" (",") "," ","-");
        options.setNumericOnly(true);
        return new CustomStringBlockFormatter(options);
    }

    @Bean(name = "innFieldFormatter")
    public static CustomStringBlockFormatter getInnFieldFormatter() {
        CustomStringBlockFormatter.Options options = new CustomStringBlockFormatter.Options();
        options.setBlocks(10);
        options.setNumericOnly(true);
        return new CustomStringBlockFormatter(options);
    }
}
