package com.example.employees.vaadin.component.grid;

import com.example.employees.vaadin.view.EntitiesCrudOnTabView;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.data.selection.SelectionListener;
import com.vaadin.flow.shared.Registration;

/**
 * Интерфейс, позволющий реализующим его классам быть использованными в {@link EntitiesCrudOnTabView} для отображения
 * в отдельных табах с возможностью добавления/удаления/редактирования и поиска сущностей E
 * */
public interface EntitiesTab<E> {
    /**
     * Возвращает заголовок таба, под которым будут отображены сущности
     * @return заголовок таба
     */
    String getTitle();
    /**
     * Устанавливает фильтр - поиск сущностей содержащих переданную строку
     * @param filter - искомая в сущностях строка
     */
    void setFilter(String filter);
    /**
     * Возвращает отображаемый в отдельном табе списковый компонент
     * @return компонент отображающий список сущностей
     */
    Component getComponent();
    /**
     * Возвращает отображаемый(-ые) рядом с кнопками добавления/удаления/редактирования компонент(-ы),
     * реализующие дополнительный функционал над сущностями из {@link EntitiesTab}
     * @return компонент доп. панель управления над сущностями
     */
    default Component getControlComponents() {
        return null;
    }
    /** Создания новой сущности. Вызывается по нажатию на нопку "Добавить" {@link EntitiesCrudOnTabView} */
    void onCreateEntity();
    /** Редактирование сущности. Вызывается по нажатию на нопку "Редактировать" {@link EntitiesCrudOnTabView} */
    void onEditEntity();
    /** Удаление сущности. Вызывается по нажатию на нопку "Удалить" {@link EntitiesCrudOnTabView} */
    void onRemoveEntity();
    /** Метод для обновления списка данных */
    void refillData();
    /**
     * Подписка на событие выбора сущности. Используется для активации/деактивации кнопок удалить и редактировать из {@link EntitiesCrudOnTabView}
     * @param listener испольняемый метод
     * @return объект, позволяющий удалить подписанных на событие слушателей
     */
    Registration addEntitySelectionListener(SelectionListener<Grid<E>, E> listener);
}
