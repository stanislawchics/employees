package com.example.employees.vaadin.component.grid;

import com.example.employees.dao.CompanyDao;
import com.example.employees.dao.EmployeeDao;
import com.example.employees.domain.Company;
import com.example.employees.domain.Employee;
import com.example.employees.vaadin.component.editor.EmployeeModalEditor;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.*;
import com.vaadin.flow.data.selection.SelectionListener;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Класс отображающий список сотрудников, и реализующий все методы {@link EntitiesTab}. Использует {@link EmployeeDao} и
 * {@link CompanyDao} для доступа к данным.
 * Содержит дополнительный компонент фильтр сотрудников по компаниям {@link EmployeeEntitiesGrid#filterByCompanies}
 */
@SpringComponent
@UIScope
public class EmployeeEntitiesGrid extends Grid<Employee> implements EntitiesTab<Employee> {
    /** Заголовок для таба {@link EntitiesTab#getTitle()} */
    @Getter
    private final String title = "Сотрудники компаний";
    /** Источник данных о сотрудниках */
    private EmployeeDao employeeDao;
    /** Компонент для редактирования сотрудника */
    private EmployeeModalEditor editor;
    /** Фильтр по сотрудникам {@link EntitiesTab#setFilter(String)} */
    private String filter = "";
    /** Источник данных о компаниях, используется {@link EmployeeEntitiesGrid#filterByCompanies} для фильтрации сотрудников по компаниям */
    private CompanyDao companyDao;
    /** Компонент - выпадающий список для фильтрации сотрудников по компаням */
    private ComboBox<Company> filterByCompanies = new ComboBox<>();
    /**
     * Мапа, используемая для получения названий компаний при отображении в списке т.к. {@link Employee} содержит только
     * id компании. Так же передается в окно редактирования {@link EmployeeEntitiesGrid#editor}, что бы то, в свою очередь, не дергало DAO
     */
    private Map<Long, Company> companiesMap = new HashMap<>();
    /** DataProvider, используемый {@link EmployeeEntitiesGrid#filterByCompanies}*/
    private ListDataProvider<Company> companiesDP;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void setFilter(String filter) {
        if (!isVisible())
            return;
        if (filter != null && !filter.equals(this.filter)) {
            this.filter = filter;
            refillData();
        }
    }

    @Autowired
    public EmployeeEntitiesGrid(EmployeeDao employeeDao, EmployeeModalEditor editor, CompanyDao companyDao) {
        super(Employee.class);
        this.employeeDao = employeeDao;
        this.editor = editor;
        this.companyDao = companyDao;
        setColumns();
        setSelectionMode(SelectionMode.MULTI);
        prepareFilterByCompanies();
    }

    private void setColumns() {
        removeAllColumns();
        addColumn(Employee::getFullName).setHeader("ФИО");
        addColumn(emp -> companiesMap.get(emp.getCompanyId()).getName()).setHeader("Компания");
        addColumn(Employee::getEmail).setHeader("E-mail");
        addColumn(Employee::getBirthday).setHeader("День рождения");
    }

    private void prepareFilterByCompanies() {
        fillCompaniesMap();
        companiesDP = DataProvider.ofCollection(companiesMap.values());
        filterByCompanies.setDataProvider(companiesDP);
        filterByCompanies.setClearButtonVisible(true);
        filterByCompanies.setItemLabelGenerator(Company::getName);
        filterByCompanies.addValueChangeListener(e -> refillData());
    }

    private void fillCompaniesMap() {
        List<Company> companies = companyDao.getCompaniesSortedByName();
        if(companies == null) {
            Notification.show("При получении списка компаний произошла ошибка", 2000, Notification.Position.MIDDLE);
            logger.error("Ошибка при получении списка отсортированных по имени компаний");
        } else {
            companies.forEach(company -> companiesMap.put(company.getId(), company));
        }
    }

    @Override
    public Component getControlComponents() {
        return filterByCompanies;
    }

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible) {
            refillData();
            companiesMap.clear();
            fillCompaniesMap();
            companiesDP.refreshAll();
        }
        super.setVisible(visible);
    }

    @Override
    public void refillData() {
        if (filter == null || filter.isEmpty()) {
            List<Employee> employees = getCompanyEmployeesOrAllEmployeesIfCompanyIsNull(filterByCompanies.getValue());
            if (employees == null) {
                Notification.show("При получении списка сотрудников произошла ошибка", 2000, Notification.Position.MIDDLE);
                logger.error("Ошибка при получении списка сотрудников без фильтра" +
                        (filterByCompanies.getValue() == null? "" : ". Выбранная компания - " + filterByCompanies.getValue()));
            } else {
                setItems(employees);
            }
        } else {
            List<Employee> employees = getCompanyEmployeesOrAllEmployeesIfCompanyIsNull(filterByCompanies.getValue(), filter);
            if (employees == null) {
                Notification.show("При получении списка сотрудников произошла ошибка", 2000, Notification.Position.MIDDLE);
                logger.error("Ошибка при получении списка сотрудников соответствующих фильтру: " + filter +
                        (filterByCompanies.getValue() == null? "" : ". Выбранная компания - " + filterByCompanies.getValue()));
            } else {
                setItems(employees);
            }
        }
    }

    private List<Employee> getCompanyEmployeesOrAllEmployeesIfCompanyIsNull(Company company) {
        if(company == null || company.getId() == null) {
            return employeeDao.getEmployees();
        } else {
            return employeeDao.getEmployeesByCompanies(List.of(company.getId()));
        }
    }

    private List<Employee> getCompanyEmployeesOrAllEmployeesIfCompanyIsNull(Company company, String filter) {
        if(company == null || company.getId() == null) {
            return employeeDao.findEmployee(filter);
        } else {
            return employeeDao.findEmployeeInCompanies(List.of(company.getId()), filter);
        }
    }

    @Override
    public void onCreateEntity() {
        if (!isVisible())
            return;
        Employee emp = new Employee();
        if(filterByCompanies.getValue() != null &&filterByCompanies.getValue().getId() != null) {
            emp.setCompanyId(filterByCompanies.getValue().getId());
        }
        editor.editEmployee(emp,this, companiesMap.values());
        editor.open();
    }

    @Override
    public void onEditEntity() {
        if (!isVisible())
            return;
        Set<Employee> selectedEmployees = getSelectedItems();
        Employee selectedEmployee = selectedEmployees.stream().findFirst().get();
        editor.editEmployee(selectedEmployee, this, companiesMap.values());
        editor.open();
    }

    @Override
    public void onRemoveEntity() {
        if (!isVisible())
            return;
        confirmToRemoveEmployees(getSelectedItems());
    }


    private void confirmToRemoveEmployees(Set<Employee> employeesToRemove) {
        Button yes = new Button("Да");
        Button no = new Button("Отмена");
        Span content;
        if(employeesToRemove.size() == 1) {
            content = new Span("Вы уверены, что хотите удалить выбранного сотрудника: " +
                    employeesToRemove.stream().findFirst().get().getFullName() +
                    "?");
        } else {
            content = new Span("Вы уверены, что хотите удалить " +
                    employeesToRemove.size() +
                    " сотрудников?");
        }
        HorizontalLayout hl = new HorizontalLayout(yes, no);
        VerticalLayout vl = new VerticalLayout(content, hl);
        Notification notification = new Notification(vl);
        yes.addClickListener(event -> {
            int removedCnt = employeeDao.removeEmployees(employeesToRemove.stream()
                    .map(Employee::getId)
                    .collect(Collectors.toList())
            );
            notification.close();
            refillData();
            if(removedCnt < 0) {
                Notification.show("При удалении сотрудников произошла ошибка", 2000, Notification.Position.MIDDLE);
                logger.error("Ошибка при удалении сотрудников: " + employeesToRemove);
            } else {
                Notification.show("Успешно удалено " + removedCnt + " сотрудников", 2000, Notification.Position.MIDDLE);
                logger.debug("Удалены сотрудники: " + employeesToRemove);
            }
        });
        no.addClickListener(event -> notification.close());
        notification.setPosition(Notification.Position.MIDDLE);
        notification.open();
    }

    @Override
    public Registration addEntitySelectionListener(SelectionListener<Grid<Employee>, Employee> listener) {
        return super.addSelectionListener(listener);
    }
}
