package com.example.employees.vaadin.component.validation;

public class EmailValidator extends com.vaadin.flow.data.validator.EmailValidator {
    public EmailValidator(String errorMessage) {
        super(errorMessage);
    }

    @Override
    protected boolean isValid(String value) {
        if ("".equals(value)) return true;
        return super.isValid(value);
    }
}
