package com.example.employees.vaadin.component.editor;


import com.example.employees.dao.CompanyDao;
import com.example.employees.domain.Company;
import com.example.employees.vaadin.component.grid.EntitiesTab;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.textfieldformatter.CustomStringBlockFormatter;

import java.util.List;

/**
 * Класс компонент, отображаемый в модальном окне, и используемый для сохранения новой/редактирования существующей компании
 */
@SpringComponent
@UIScope
public class CompanyModalEditor extends Dialog {
    /** Binder, связывающий поля модального окна с полями сущности {@link Company} */
    private Binder<Company> binder = new Binder<>(Company.class);
    /** Источник данных о компаниях */
    private CompanyDao companyDao;
    /** Объект компании используемый {@link CompanyModalEditor#binder} */
    private Company company;
    /** Поле с названием новой/редактируемой компании */
    private TextField name = new TextField("Название");
    /** Поле с ИНН новой/редактируемой компании */
    private TextField inn = new TextField("ИНН");
    /** Поле с адресом новой/редактируемой компании */
    private TextField address = new TextField("Адрес");
    /** Поле с телефоном новой/редактируемой компании */
    private TextField phone = new TextField("Телефон");
    /** Кнопка сохранить, вызывает {@link CompanyModalEditor#saveCompany()} */
    private Button btnSave = new Button("Сохранить");
    /** Кнопка отменить, закрывающая модальное окно */
    private Button btnCancel = new Button("Отменить");
    private HorizontalLayout btns = new HorizontalLayout(btnSave, btnCancel);
    /** Ссылка на компонент - список компаний, используемая для обновления списка после сохранения компании */
    private EntitiesTab companiesGrid;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Конструктор. Содержит вызов метода для разхмещения полей и кнопок,
     * а так же вызов метода для связывания и валидации полей с {@link CompanyModalEditor#binder}
     * @param companyDao - источник данных о компаниях
     * @param phoneFieldFormatter - маска для телефона
     * @param innFieldFormatter - маска для ИНН
     */
    @Autowired
    public CompanyModalEditor(CompanyDao companyDao, CustomStringBlockFormatter phoneFieldFormatter, CustomStringBlockFormatter innFieldFormatter) {
        this.companyDao = companyDao;
        addButtonsAndTextField();
        bindFields(phoneFieldFormatter, innFieldFormatter);
     }

    private void addButtonsAndTextField() {
        HorizontalLayout companyFieldsLayout = new HorizontalLayout(name, inn, address, phone);
        VerticalLayout layout = new VerticalLayout(companyFieldsLayout, btns);
        add(layout);
        btnSave.addClickListener(e -> saveCompany());
        btnCancel.addClickListener(e -> close());
    }

    private void bindFields(CustomStringBlockFormatter phoneFieldFormatter, CustomStringBlockFormatter innFieldFormatter) {
        binder.forField(name)
                .asRequired("Название не должно быть пустым")
                .bind(Company::getName, Company::setName);
        binder.forField(inn)
                .withValidator(inn -> inn.length() == 0 || inn.replaceAll("[^0-9]", "").length() == 10,
                        "Инн должен состоять из 10 цифр")
                .bind(Company::getInn, (c, inn) -> c.setInn(inn.replaceAll("[^0-9]", "")));
        binder.forField(phone)
                .withValidator(phone -> phone.length() == 0 || phone.replaceAll("[^0-9]", "").length() == 11,
                        "Телефон должен состоять из 11 цифр")
                .bind(Company::getPhone, (c, phone) -> c.setPhone(phone.replaceAll("[^0-9]", "")));
        binder.bindInstanceFields(this);
        phoneFieldFormatter.extend(phone);
        innFieldFormatter.extend(inn);
    }

    private void saveCompany() {
        if (binder.validate().hasErrors())
            return;
        int updatedCnt = companyDao.updateOrInsertCompany(company);
        if (updatedCnt < 0) {
            String action = company.getId() == null ? "добавлении" : "изменении";
            Notification.show("При " + action + " компании " + company.getName() + " произошла ошибка", 2000, Notification.Position.MIDDLE);
            logger.error("Ошибка при " + action + " компании - " + company);
        } else {
            String action = company.getId() == null ? "добавлена" : "изменена";
            Notification.show("Компания " + company.getName() + " успешно " + action, 2000, Notification.Position.MIDDLE);
            logger.debug("Компания " + action + ": " + company);
        }
        companiesGrid.refillData();
        close();
    }

    /**
     * Метод инициализирует связанный с биндером объект компании
     * @param company - объект компании. Пустой в случае добавления компании, и, содержащий как минимум id, в случае с редактированием имеющейся
     * @param companiesGrid - {@link CompanyModalEditor#companiesGrid}
     */
    public void editCompany(Company company, EntitiesTab companiesGrid) {
        if (company == null) {
            close();
            return;
        }
        this.companiesGrid = companiesGrid;

        if (company.getId() != null) {
            List<Company> companies = companyDao.getCompaniesById(List.of(company.getId()));
            if (companies == null || companies.size() == 0) {
                Notification.show("Произошла ошибка - редактируемая компания не найдена. Пожалуйста, обновите страницу",
                        2000,
                        Notification.Position.MIDDLE);
                logger.error("Ошибка при получении компании по id " + company.getId());
            } else {
                this.company = companies.get(0);
            }
        } else {
            this.company = company;
        }

        binder.setBean(this.company);
        name.focus();
    }
}
