package com.example.employees.vaadin.view;

import com.example.employees.vaadin.component.grid.EntitiesTab;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import lombok.AccessLevel;
import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Компонент отображающий компоненты наследники {@link EntitiesTab} - списки произвольных сущностей,
 * с возможностью добавления/удаления/редактирования и поиска по ним.
 */
public abstract class EntitiesCrudOnTabView extends VerticalLayout {
    /** Кнопка добавить. Логику добавления реализуют наследники {@link EntitiesTab} */
    private Button btnNew = new Button("Добавить");
    /** Кнопка редактировать. Логику редактирования реализуют наследники {@link EntitiesTab} */
    private Button btnEdit = new Button("Редактировать");
    /** Кнопка удалить. Логику удаления реализуют наследники {@link EntitiesTab} */
    private Button btnDelete = new Button("Удалить");
    /** Дополнительные кнопки управления, которые может иметь наследник {@link EntitiesTab} */
    private Div componentsControlPanel = new Div();
    /** Текстовое поле для фильтрации. Логику фильтрации реализуют наследники {@link EntitiesTab} */
    private TextField search = new TextField("", "Поиск");
    private HorizontalLayout controlPanel = new HorizontalLayout(btnNew, btnEdit, btnDelete, componentsControlPanel, search);
    /** Мапа содержащая все компоненты наследники {@link EntitiesTab} */
    @Getter(AccessLevel.PROTECTED)
    private Map<Tab, EntitiesTab> tabComponents = new HashMap<>();
    @Getter(AccessLevel.PROTECTED)
    private Tabs tabs = new Tabs();

    /** Конструктор. Содержит вызов метода размещающего кнопки управления сущностями */
    public EntitiesCrudOnTabView() {
        addControlPanel();
    }

    /**
     * Метод добавляющий компоненты наследников {@link EntitiesTab} на страницы скрываемые табами
     * @param components - список отображаемых в табах компонентов
     */
    public void addTabComponents(List<EntitiesTab> components) {
        addComponents(components);
    }

    private void addControlPanel() {
        btnEdit.setEnabled(false);
        btnDelete.setEnabled(false);
        add(controlPanel);
        search.setValueChangeMode(ValueChangeMode.EAGER);

        search.addValueChangeListener(e -> tabComponents.get(tabs.getSelectedTab()).setFilter(e.getValue()));
        btnNew.addClickListener(e -> tabComponents.get(tabs.getSelectedTab()).onCreateEntity());
        btnEdit.addClickListener(e -> tabComponents.get(tabs.getSelectedTab()).onEditEntity());
        btnDelete.addClickListener(e -> tabComponents.get(tabs.getSelectedTab()).onRemoveEntity());
    }

    private void addComponents(List<EntitiesTab> components) {
        Div componentsData = new Div();
        componentsData.setWidthFull();
        for (EntitiesTab component: components) {
            Tab tab = new Tab(component.getTitle());
            tabs.add(tab);
            componentsData.add(component.getComponent());
            if(component.getControlComponents() != null) {
                componentsControlPanel.add(component.getControlComponents());
            }
            tabComponents.put(tab, component);
        }
        add(tabs);
        add(componentsData);
        hideTabComponentsExceptFirst(components);
        addTabsDefaultBehavior();
        addEntitySelectBehaviour();
    }

    private void hideTabComponentsExceptFirst(List<EntitiesTab> components) {
        boolean firstIteration = true;
        for (EntitiesTab component: components) {
            if (firstIteration) {
                component.getComponent().setVisible(true);
                if(component.getControlComponents() != null) {
                    component.getControlComponents().setVisible(true);
                }
                firstIteration = false;
            } else {
                component.getComponent().setVisible(false);
                if(component.getControlComponents() != null) {
                    component.getControlComponents().setVisible(false);
                }
            }
        }
    }

    private void addTabsDefaultBehavior() {
        tabs.addSelectedChangeListener(event -> {
            btnEdit.setEnabled(false);
            btnDelete.setEnabled(false);
            EntitiesTab componentToShow = tabComponents.get(tabs.getSelectedTab());
            tabComponents.values().forEach(c -> {
                if(c.equals(componentToShow)) {
                    c.getComponent().setVisible(true);
                    if(c.getControlComponents() != null) {
                        c.getControlComponents().setVisible(true);
                    }
                    c.setFilter("");
                } else {
                    c.getComponent().setVisible(false);
                    if(c.getControlComponents() != null) {
                        c.getControlComponents().setVisible(false);
                    }
                }
            });
            search.setValue("");
        });
    }

    private void addEntitySelectBehaviour() {
        tabComponents.values().forEach(c ->
            c.addEntitySelectionListener(e -> {
                if(e.getAllSelectedItems().size() == 0) {
                    btnEdit.setEnabled(false);
                    btnDelete.setEnabled(false);
                } else if(e.getAllSelectedItems().size() == 1) {
                    btnEdit.setEnabled(true);
                    btnDelete.setEnabled(true);
                } else if(e.getAllSelectedItems().size() > 1) {
                    btnEdit.setEnabled(false);
                    btnDelete.setEnabled(true);
                }
            })
        );
    }
}
