package com.example.employees.vaadin.view;

import com.example.employees.vaadin.component.grid.CompanyEntitiesGrid;
import com.example.employees.vaadin.component.grid.EmployeeEntitiesGrid;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

/**
 * Компонент наследник {@link EntitiesCrudOnTabView}, отображающий список компаний {@link CompanyEntitiesGrid} и
 * сотрудников {@link EmployeeEntitiesGrid}
 */
@Route("")
public class CompAndEmpCrudOnTabView extends EntitiesCrudOnTabView {

    /**
     * Конструктор. Содержит вызов метода {@link EntitiesCrudOnTabView#addTabComponents(List)}
     * @param companyEntities - компонент отображающий компаний
     * @param employeeEntities - компонент отображающий сотрудников
     */
    @Autowired
    public CompAndEmpCrudOnTabView(CompanyEntitiesGrid companyEntities, EmployeeEntitiesGrid employeeEntities) {
        addTabComponents(List.of(companyEntities, employeeEntities));
    }
}
